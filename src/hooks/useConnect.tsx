import React, { ReactElement, FC } from 'react';

const HOC = <T, M>(Component: FC<T>, select: () => M): FC<T> => {
  const HOC = (props: T): ReactElement<T> => {
    const selector = select();
    return <Component {...selector} {...props} />;
  };
  return HOC;
};

export default HOC;
