export const BUCKET_SELECTION = 'BUCKET_SELECTION';
export const TOGGLE_BEAN_MACHINE = 'TOGGLE_BEAN_MACHINE';
export const RESET_BEAN_MACHINE = 'RESET_BEAN_MACHINE';
export const INCREASE_BUCKET_SIZE = 'INCREASE_BUCKET_SIZE';

type BucketSelectionType = { type: typeof BUCKET_SELECTION; bucket: number };
type ToggleBeanMachineType = { type: typeof TOGGLE_BEAN_MACHINE };
type ResetBeanMachineType = { type: typeof RESET_BEAN_MACHINE };
type IncreaseBucketSizeType = { type: typeof INCREASE_BUCKET_SIZE; bucket: number };

export const BucketSelection = (bucket: number): BucketSelectionType => {
  return { type: BUCKET_SELECTION as typeof BUCKET_SELECTION, bucket };
};

export const ToggleBeanMachine = (): ToggleBeanMachineType => {
  return { type: TOGGLE_BEAN_MACHINE as typeof TOGGLE_BEAN_MACHINE };
};

export const ResetBeanMachine = (): ResetBeanMachineType => {
  return { type: RESET_BEAN_MACHINE as typeof RESET_BEAN_MACHINE };
};

export const IncreaseBucketSize = (bucket: number): IncreaseBucketSizeType => {
  return { type: INCREASE_BUCKET_SIZE as typeof INCREASE_BUCKET_SIZE, bucket };
};

export type Actions =
  | BucketSelectionType
  | ToggleBeanMachineType
  | ResetBeanMachineType
  | IncreaseBucketSizeType;
