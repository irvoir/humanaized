export const WIDTH = 800;
export const HEIGHT = 600;

export const COLS = 20;
export const ROWS = 20;
export const MARGIN_BOTTOM = 125;

export const BEAN_RAD = 7;
export const MAX_BEANS = 100;
