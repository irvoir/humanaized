export const BACKGROUND = '#002B36';

export const VIEW = '#268BD2';
export const CONTROL = '#2AA198';
export const PEGS = '#2AA198';
export const BEANS = '#990000';
export const BUCKETS = '#623B32';
export const DARK = '#7474a8';

export const SENSORS = '#B58900';
export const BEAM = '#B58900';
export const WOOD = '#B58900';

export const BUCKET_TEXT = '#2AA198';

export const SPARKLINE_GRADIENT_STROKE = '#fff';
export const SPARKLINE_GRADIENT_TOP = '#002B56';
export const SPARKLINE_GRADIENT_BOTTOM = '#002B36';
