import React, { ReactElement, useMemo, useReducer } from 'react';
import { GlobalStore, GlobalDispatch } from './store/state';
import { reducer, initialState } from './store/store';
import Home from './views/Home/Home';
import Control from './views/Control/Control';
import Plot from './views/Plot/Plot';
import { GlobalStyle } from './AppStyles';

export const App = (): ReactElement => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const memoState = useMemo(() => state, [state]);
  const memoDispatch = useMemo(() => ({ dispatch }), [dispatch]);
  return (
    <GlobalStore.Provider value={memoState}>
      <GlobalDispatch.Provider value={memoDispatch}>
        <GlobalStyle />
        <Home />
        <Control />
        <Plot />
      </GlobalDispatch.Provider>
    </GlobalStore.Provider>
  );
};
