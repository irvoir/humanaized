import { GlobalState } from './types';

import {
  BUCKET_SELECTION,
  TOGGLE_BEAN_MACHINE,
  INCREASE_BUCKET_SIZE,
  RESET_BEAN_MACHINE,
  Actions,
} from '../actions/actions';

export const initialState: GlobalState = {
  isRunning: false,
  total: 0,
  maxBeans: 0,
  selectedBucket: null,
  reset: false,
  plots: [],
  buckets: Array(10).fill(0),
};

export const reducer = (state = initialState, action: Actions): GlobalState => {
  switch (action.type) {
    case INCREASE_BUCKET_SIZE: {
      const { buckets } = state;
      const bucketCopy = buckets.slice();
      bucketCopy[action.bucket]++;
      return {
        ...state,
        buckets: bucketCopy,
        total: state.total + 1,
      };
    }
    case RESET_BEAN_MACHINE: {
      return {
        ...state,
        buckets: Array(10).fill(0),
        isRunning: false,
        reset: true,
        total: 0,
      };
    }
    case TOGGLE_BEAN_MACHINE: {
      return {
        ...state,
        isRunning: !state.isRunning,
        selectedBucket: 0,
        reset: false,
      };
    }
    case BUCKET_SELECTION: {
      const { buckets, plots } = state;
      const plotsCopy = plots.slice();
      const bucketCopy = buckets.slice();
      if (plots.length < 10) {
        plotsCopy.push(bucketCopy);
      }
      return {
        ...state,
        selectedBucket: action.bucket,
        maxBeans: buckets[action.bucket],
        isRunning: false,
        plots: plotsCopy,
      };
    }
    default:
      return state;
  }
};
