export interface GlobalState {
  buckets: number[];
  isRunning: boolean;
  total: number;
  maxBeans: number;
  selectedBucket: number | null;
  reset: boolean;
  plots: number[][];
}

export interface GlobalDispatch<T> {
  dispatch: T;
}
