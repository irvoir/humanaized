import { createContext } from 'react';
import { GlobalState as GlobalStoreProps, GlobalDispatch as GlobalDispatchProps } from './types';
import { initialState } from './store';
import { Actions } from '../actions/actions';

export const GlobalStore = createContext<GlobalStoreProps>(initialState);
export const GlobalDispatch = createContext({} as GlobalDispatchProps<React.Dispatch<Actions>>);
