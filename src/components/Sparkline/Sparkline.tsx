import React, { useState, useRef, ReactElement, useLayoutEffect } from 'react';
import getY from '../../helpers/getY';
import { SparklineProps } from './types';
import {
  SPARKLINE_GRADIENT_STROKE,
  SPARKLINE_GRADIENT_TOP,
  SPARKLINE_GRADIENT_BOTTOM,
} from '../../constants/colors';

const Sparkline = ({ data, width, height }: SparklineProps): ReactElement => {
  const [dims, setDims] = useState<{ width: number; height: number }>({ width: 0, height: 0 });
  const [coords, setCoords] = useState<{ line: string; fill: string }>({ line: '', fill: '' });
  const graphRef = useRef<SVGSVGElement>(null);

  useLayoutEffect(() => {
    if (graphRef && graphRef.current !== null) {
      const { width: svgWidth, height: svgHeight } = graphRef.current.getBoundingClientRect();
      setDims({ width: svgWidth, height: svgHeight });
      const max = Math.max(...data);
      const y = getY(max, svgHeight, 0, data[0]);
      let pathCoords = `M0 ${isNaN(y) ? svgHeight : y}`;
      for (let index = 0; index <= data.length; index += 1) {
        const x = (index * svgWidth) / data.length;
        const y = getY(max, svgHeight - 20, 20, data[index]);
        console.log(x, y);
        pathCoords += ` L ${x} ${isNaN(y) ? svgHeight : y}`;
      }
      const fillCoords = `${pathCoords} V ${svgHeight} L 0 ${svgHeight}`;
      setCoords({ line: pathCoords, fill: fillCoords });
    }
  }, [data, setDims]);

  return (
    <svg ref={graphRef} width={width} height={height}>
      <GradientDefs />
      <svg
        width={dims.width}
        height={dims.height}
        strokeWidth={2}
        stroke={SPARKLINE_GRADIENT_STROKE}
        strokeLinejoin="round"
      >
        <path d={coords.fill} fill="url(#gradient)" stroke="none" />
        <path d={coords.line} fill="none" />
      </svg>
    </svg>
  );
};

const GradientDefs = (): ReactElement => (
  <defs>
    <linearGradient id="gradient" x2="0%" y2="100%">
      <stop offset="0%" stopColor={SPARKLINE_GRADIENT_TOP} />
      <stop offset="100%" stopColor={SPARKLINE_GRADIENT_BOTTOM} />
    </linearGradient>
  </defs>
);

Sparkline.defaultProps = {
  width: 800,
  height: 150,
};

export default Sparkline;
