export interface SparklineProps {
  data: number[];
  width?: number;
  height: number;
}
