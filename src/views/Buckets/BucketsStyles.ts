import styled from 'styled-components';
import { BUCKET_TEXT } from '../../constants/colors';

export const Wrapper = styled.div`
  grid-area: buckets;
  align-self: start;
  display: grid;
  grid-template-columns: repeat(10, 80px);
  justify-items: center;
`;

export const BucketSpan = styled.span`
  font-size: 1.75em;
  cursor: default;
  text-align: center;
  color: ${BUCKET_TEXT};
`;
