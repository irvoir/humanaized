import React, { memo, FC, useCallback, useContext } from 'react';
import { Wrapper, BucketSpan } from './BucketsStyles';
import { BucketSelection, ResetBeanMachine } from '../../actions/actions';
import { GlobalStore, GlobalDispatch } from '../../store/state';
import { BucketsProps } from './types';
import UseConnect from '../../hooks/useConnect';

const Buckets: FC<BucketsProps> = memo(({ buckets, dispatch }: BucketsProps) => {
  const selectBucket = useCallback(
    (bucket) => {
      if (bucket && buckets[bucket] !== 0) {
        dispatch(BucketSelection(bucket));
        dispatch(ResetBeanMachine());
      }
    },
    [dispatch, buckets],
  );
  return (
    <Wrapper>
      {Array(10)
        .fill(0)
        .map((value: number, index: number) => (
          <BucketSpan key={value + index} onClick={(): void => selectBucket(index)}>
            {buckets[index]}
          </BucketSpan>
        ))}
    </Wrapper>
  );
});

Buckets.displayName = 'Buckets';

const Selector = (): BucketsProps => {
  const { buckets } = useContext(GlobalStore);
  const { dispatch } = useContext(GlobalDispatch);
  return { buckets, dispatch };
};

export default UseConnect<BucketsProps, BucketsProps>(Buckets, Selector);
