import { Actions } from '../../actions/actions';

export interface BucketsProps {
  buckets: number[];
  dispatch: (value: Actions) => void;
}
