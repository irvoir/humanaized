import React, { FC, memo, useState, useRef, useLayoutEffect, useEffect, useContext } from 'react';
import Matter, {
  World as MatterWorld,
  Engine as MatterEngine,
  Render as MatterRender,
  Body as MatterBody,
} from 'matter-js';
import useInterval from '../../hooks/useInterval';
import createBeans from '../../helpers/createBeans';
import createPegs from '../../helpers/createPegs';
import createWood from '../../helpers/createWood';
import createBeams from '../../helpers/createBeams';
import createSensors from '../../helpers/createSensors';
import randomBetween from '../../helpers/randomBetween';
import UseConnect from '../../hooks/useConnect';
import { BACKGROUND } from '../../constants/colors';
import { BeanMachine } from './CanvasStyles';
import { IncreaseBucketSize } from '../../actions/actions';
import { GlobalStore, GlobalDispatch } from '../../store/state';
import {
  MARGIN_BOTTOM,
  COLS,
  ROWS,
  WIDTH,
  HEIGHT,
  MAX_BEANS,
  BEAN_RAD,
} from '../../constants/canvas';
import { CanvasProps, LocalPair } from './types';

const { Engine, Render, Events, World } = Matter;

const Canvas: FC<CanvasProps> = memo(({ reset, isRunning, maxBeans, dispatch }: CanvasProps) => {
  const [hasStarted, setHasStarted] = useState<boolean>(false);
  const [limit, setLimit] = useState<number>(0);
  const canvas = useRef<HTMLDivElement>(null);
  const CanvasRender = useRef<MatterRender>();
  const CanvasEngine = useRef<MatterEngine>();
  const CanvasWorld = useRef<MatterWorld>();

  useEffect(() => {
    if (reset && CanvasWorld.current && CanvasRender.current && CanvasEngine.current) {
      Events.off(CanvasEngine.current, 'collisionEnd', () => null);
      World.clear(CanvasEngine.current.world, false);
      Engine.clear(CanvasEngine.current);
      CanvasRender.current.canvas.remove();
      setHasStarted(false);
      setLimit(0);
    }
  }, [reset]);

  // This runs synchronously immediately after React has performed all DOM mutations.
  useLayoutEffect(() => {
    if (!hasStarted) {
      CanvasEngine.current = Engine.create();
      CanvasRender.current = Render.create({
        element: canvas.current as HTMLDivElement,
        engine: CanvasEngine.current,
        options: {
          wireframes: false,
          hasBounds: true,
          background: BACKGROUND,
        },
      });

      const colSize = WIDTH / COLS;
      const rowSize = (HEIGHT - MARGIN_BOTTOM) / ROWS;
      const PADDING_TOP = rowSize * 2;

      // Create the pegs where the beans can bounce off if they collide
      const grid: MatterBody[][] | number[][] = [];

      for (let i = 0; i < ROWS; i++) {
        grid[i] = [];
        let mid: number = (COLS + i) >> 1;
        for (let l = i; l > 0; l--, mid--) {
          const x = colSize * mid - (i & 1 ? 0 : colSize / 2);
          const y = rowSize * i + PADDING_TOP;
          grid[i][mid] = createPegs(x, y, 4);
        }
      }

      const pegs = grid.flat();

      // Create the bucket beams, so they can be guided to touch the sensor.
      const beams = Array.from(Array(11).keys()).map((value: number) =>
        createBeams(WIDTH, HEIGHT, value),
      );

      const sensors = Array.from(Array(10).keys()).map((value: number) =>
        createSensors(WIDTH, HEIGHT, value),
      );

      const { world } = CanvasEngine.current;
      World.add(world, [...createWood(WIDTH), ...pegs, ...beams, ...sensors]);
      Engine.run(CanvasEngine.current);
      Render.run(CanvasRender.current);
      Events.on(CanvasEngine.current, 'collisionEnd', (event) => {
        const pairs = event.pairs.slice();
        const [left, right] = pairs as [LocalPair, LocalPair];
        if (left && left.isSensor) {
          const { bodyA, bodyB } = left;
          if (parseInt(bodyA.label) != NaN) {
            World.remove(world, bodyB);
            dispatch(IncreaseBucketSize(parseInt(bodyA.label)));
          }
        }
        if (right && right.isSensor) {
          const { bodyA, bodyB } = right;
          if (parseInt(bodyA.label) != NaN) {
            World.remove(world, bodyB);
            dispatch(IncreaseBucketSize(parseInt(bodyA.label)));
          }
        }
      });
      setHasStarted(true);
      CanvasWorld.current = world;
    }
  }, [hasStarted, dispatch, maxBeans]);

  const freeFall = maxBeans === 0 && isRunning;
  const isLimited = maxBeans > 0 && isRunning;

  // Free fall mode, means that it will continue to spawn beans infinitely
  // But it will clean itself if exists more than MAX_BEANS in 'World'
  useInterval(
    () => {
      if (CanvasWorld.current) {
        World.add(
          CanvasWorld.current,
          createBeans(randomBetween(WIDTH / 2 - 75, WIDTH / 2 + 75), 0, BEAN_RAD),
        );
        const existingBeans = CanvasWorld.current.bodies.filter(
          (body: { label: string }) => body.label === 'bean',
        );
        if (maxBeans !== 0 && existingBeans.length > MAX_BEANS) {
          World.remove(CanvasWorld.current, existingBeans[0]);
        }
      }
    },
    freeFall && !isLimited ? 1000 : null,
  );

  // Will run until the maxBeans limit is reached
  useInterval(
    () => {
      if (CanvasWorld.current && limit < maxBeans) {
        World.add(
          CanvasWorld.current,
          createBeans(randomBetween(WIDTH / 2 - 75, WIDTH / 2 + 75), 0, BEAN_RAD),
        );
        setLimit((v) => v + 1);
      }
    },
    !freeFall && isLimited ? 1000 : null,
  );

  useEffect(() => {
    return (): void => {
      // Clean up the event listener in case the canvas is unmounted;
      Events.off(CanvasEngine.current, 'collisionEnd', () => null);
    };
  });

  return (
    <BeanMachine>
      <div ref={canvas} />
    </BeanMachine>
  );
});

Canvas.displayName = 'Canvas';

const Selector = (): CanvasProps => {
  const { isRunning, reset, maxBeans } = useContext(GlobalStore);
  const { dispatch } = useContext(GlobalDispatch);
  return { isRunning, reset, maxBeans, dispatch };
};

export default UseConnect<CanvasProps, CanvasProps>(Canvas, Selector);
