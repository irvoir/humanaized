import { Actions } from '../../actions/actions';
import { IPair as Pair } from 'matter-js';

export interface CanvasProps {
  reset: boolean;
  maxBeans: number;
  isRunning: boolean;
  dispatch: (value: Actions) => void;
}

export interface LocalPair extends Pair {
  isSensor: boolean;
}
