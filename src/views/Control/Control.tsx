import React, { memo, FC, useCallback, useContext } from 'react';
import { Wrapper, ControlRow, Title, Symbol } from './ControlStyles';
import { ToggleBeanMachine } from '../../actions/actions';
import { GlobalStore, GlobalDispatch } from '../../store/state';
import UseConnect from '../../hooks/useConnect';
import { ControlProps } from './types';

const Control: FC<ControlProps> = memo(
  ({ isRunning, total, selectedBucket, maxBeans, dispatch }: ControlProps) => {
    const startOrStop = useCallback(() => {
      dispatch(ToggleBeanMachine());
    }, [dispatch]);
    return (
      <Wrapper>
        <Title>Data</Title>
        <ControlRow>
          Max beans:
          <Symbol>{maxBeans === 0 ? '✖' : maxBeans}</Symbol>
        </ControlRow>
        <ControlRow>
          Total:
          <Symbol>{total}</Symbol>
        </ControlRow>
        <Title>Status</Title>
        <ControlRow>
          {isRunning ? 'Running' : 'Stopped'}
          <Symbol>{isRunning ? '⦿' : '⦻'}</Symbol>
        </ControlRow>
        <ControlRow>
          Bucket:
          <Symbol>{selectedBucket ? selectedBucket + 1 : '✖'}</Symbol>
        </ControlRow>
        <Title>Control</Title>
        <ControlRow onClick={startOrStop}>
          <Title>{isRunning ? 'Stop' : 'Start'}</Title>
          <Symbol>{isRunning ? '✖' : '►'}</Symbol>
        </ControlRow>
      </Wrapper>
    );
  },
);

Control.displayName = 'Control';

const Selector = (): ControlProps => {
  const { isRunning, total, selectedBucket, maxBeans } = useContext(GlobalStore);
  const { dispatch } = useContext(GlobalDispatch);
  return { isRunning, total, selectedBucket, maxBeans, dispatch };
};

export default UseConnect<ControlProps, ControlProps>(Control as FC<any>, Selector);
