import { Actions } from '../../actions/actions';

export interface ControlProps {
  isRunning: boolean;
  total: number;
  selectedBucket: number | null;
  maxBeans: number;
  dispatch: (value: Actions) => void;
}
