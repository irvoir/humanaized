import styled from 'styled-components';
import { CONTROL, VIEW } from '../../constants/colors';

export const Wrapper = styled.div`
  position: absolute;
  display: grid;
  z-index: 0;
  left: 0;
  top: 0;
  width: 150px;
  height: 100vh;
  background: ${CONTROL};
  box-shadow: 3px -1px 5px 0px rgba(0, 0, 0, 0.15);
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-template-rows: repeat(auto-fill, minmax(50px, 1fr));
  grid-gap: 10px;
  padding: 30px 15px 0px 15px;
`;

export const Title = styled.div`
  text-transform: uppercase;
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
  align-content: center;
`;

export const ControlRow = styled.div`
  background-color: ${VIEW};
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-radius: 5px;
  padding: 0px 15px 0px 15px;
  cursor: default;
`;

export const Symbol = styled.div`
  font-size: 1.5em;
`;
