import { Actions } from '../../actions/actions';

export interface PlotProps {
  plots: number[][];
  dispatch: (value: Actions) => void;
}
