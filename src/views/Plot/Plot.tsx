import React, { useContext, memo, ReactElement, FC } from 'react';
import { Wrapper, Title, PlotRow } from './PlotStyles';
import Sparkline from '../../components/Sparkline/Sparkline';
import { GlobalStore, GlobalDispatch } from '../../store/state';
import UseConnect from '../../hooks/useConnect';
import { PlotProps } from './types';

const Plot: FC<PlotProps> = memo(
  ({ plots }: PlotProps): ReactElement => {
    return (
      <Wrapper>
        <Title>Plots</Title>
        {plots.map(
          (bucket: number[], index: number): ReactElement => (
            <PlotRow key={index}>
              <Sparkline data={bucket} width={100} height={50} />
              <span>{index + 1}#</span>
            </PlotRow>
          ),
        )}
      </Wrapper>
    );
  },
);

Plot.displayName = 'Plot';

const Selector = (): PlotProps => {
  const { plots } = useContext(GlobalStore);
  const { dispatch } = useContext(GlobalDispatch);
  return { plots, dispatch };
};

export default UseConnect<PlotProps, PlotProps>(Plot as FC<any>, Selector);
