import styled from 'styled-components';

export const Wrapper = styled.div`
  grid-area: graph;
  display: flex;
  justify-self: center;
  align-items: center;
  align-content: center;
`;
