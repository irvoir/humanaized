import { Actions } from '../../actions/actions';

export interface TopGraphProps {
  buckets: number[];
  dispatch: (value: Actions) => void;
}
