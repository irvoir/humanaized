import React, { useContext, memo, FC } from 'react';
import Sparkline from '../../components/Sparkline/Sparkline';
import { GlobalStore, GlobalDispatch } from '../../store/state';
import { Wrapper } from './TopGraphStyles';
import UseConnect from '../../hooks/useConnect';

import { TopGraphProps } from './types';

const TopGraph: FC<TopGraphProps> = memo(({ buckets }: TopGraphProps) => {
  return (
    <Wrapper>
      <Sparkline width={800} data={buckets} />
    </Wrapper>
  );
});

TopGraph.displayName = 'TopGraph';

const Selector = (): TopGraphProps => {
  const { buckets } = useContext(GlobalStore);
  const { dispatch } = useContext(GlobalDispatch);
  return { buckets, dispatch };
};

export default UseConnect<TopGraphProps, TopGraphProps>(TopGraph, Selector);
