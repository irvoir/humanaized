import styled from 'styled-components';
import { BACKGROUND } from '../../constants/colors';

export const HomeContent = styled.div`
  display: grid;
  position: relative;
  width: 100vw;
  height: 100vh;
  background-color: ${BACKGROUND};
  justify-content: center;
  align-content: center;
  align-items: center;
  justify-items: center;
  grid-template-columns: 150px 1fr 150px;
  grid-template-rows: 150px 1fr 100px;
  grid-template-areas:
    '. graph     .'
    '. canvas    .'
    '. buckets   .';
`;
