import React, { ReactElement } from 'react';
import { HomeContent } from './HomeStyles';
import TopGraph from '../../views/TopGraph/TopGraph';
import Canvas from '../../views/Canvas/Canvas';
import Buckets from '../Buckets/Buckets';

const Home = (): ReactElement => (
  <HomeContent>
    <TopGraph />
    <Canvas />
    <Buckets />
  </HomeContent>
);

export default Home;
