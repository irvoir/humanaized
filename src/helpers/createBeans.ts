import { Bodies, Body } from 'matter-js';
import { BEANS } from '../constants/colors';

const createBeans = (x: number, y: number, r: number): Body =>
  Bodies.circle(x, y, r, {
    restitution: 0.7,
    mass: 1,
    render: {
      fillStyle: BEANS,
    },
    label: 'bean',
  });

export default createBeans;
