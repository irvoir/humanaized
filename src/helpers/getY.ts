export default (max: number, height: number, diff: number, value: number): number =>
  height - (value * height) / max + diff;
