import { Bodies, Body } from 'matter-js';
import { PEGS } from '../constants/colors';

export default (x: number, y: number, r: number): Body =>
  Bodies.circle(x, y, r, {
    isStatic: true,
    render: { fillStyle: PEGS },
    label: 'peg',
  });
