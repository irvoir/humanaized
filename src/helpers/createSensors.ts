import { Bodies, Body } from 'matter-js';

export default (x: number, y: number, value: number): Body => {
  const width = 75;
  const height = 5;
  const calcX = height * 8 + ((x - value) / 10) * value;
  const calcY = y - height / 2;
  return Bodies.rectangle(calcX, calcY, width, height, {
    isStatic: true,
    isSensor: true,
    render: {
      fillStyle: 'transparent',
    },
    label: value.toString(),
  });
};
