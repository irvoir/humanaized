import { Bodies, Body } from 'matter-js';
import { BEAM } from '../constants/colors';

export default (x: number, y: number, value: number): Body => {
  const width = 5;
  const height = 75;
  const calcX = width + ((x - value) / 10) * value;
  const calcY = y - height / 2;
  return Bodies.rectangle(calcX, calcY, width, height, {
    isStatic: true,
    render: {
      fillStyle: BEAM,
    },
  });
};
