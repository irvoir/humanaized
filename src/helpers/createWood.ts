import { Bodies, Body } from 'matter-js';
import { WOOD } from '../constants/colors';

export default (width: number): Body[] => {
  const topLeftWood = Bodies.rectangle(width / 2 - 80, 0, 175, 5, {
    isStatic: true,
    angle: Math.PI * 0.25,
    render: {
      fillStyle: WOOD,
    },
  });

  const topRightWood = Bodies.rectangle(width / 2 + 80, 0, 175, 5, {
    isStatic: true,
    angle: -Math.PI * 0.25,
    render: {
      fillStyle: WOOD,
    },
  });

  return [topLeftWood, topRightWood];
};
